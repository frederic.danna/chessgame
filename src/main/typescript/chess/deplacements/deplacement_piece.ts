import { Case } from '../echiquier/case';

export class DeplacementPiece {

    private _caseAvant: Case
    public get caseAvant(): Case {
        return this._caseAvant;
    }

    private _caseApres: Case
    public get caseApres(): Case {
        return this._caseApres;
    }


    constructor(caseAvant: Case, caseApres: Case) {

        // TODO Réactiver ce contrôle, une fois trouvé comment ne pas planter à l'initialisation des cases (avant de placer les pièces sur l'échiquier).
        // if (caseAvant.piece == undefined) {
        //     throw new Error("Case " + caseAvant.emplacement + " inoccupée (pas de pièce dessus).")
        // }

        if (caseApres.piece != undefined) {
            throw new Error("Case " + caseApres.emplacement + " déjà occupée (non libre).")
        }

        this._caseAvant = caseAvant
        this._caseApres = caseApres
    }


    /**
     * @see valider
     */
    public estValide(): boolean {
        if (this._caseAvant.piece == undefined) {
            return false
        }
        if (this._caseApres.piece != undefined) {
            return false
        }
        // TODO Coder (API ?)
        return true
    }

    /**
     * @see estValide
     */
    public valider(): void {
        if (this._caseAvant.piece == undefined) {
            throw new Error("Case " + this._caseAvant.emplacement + " inoccupée (pas de pièce dessus).")
        }
        if (this._caseApres.piece != undefined) {
            throw new Error("Case " + this._caseApres.emplacement + " déjà occupée (non libre).")
        }
        // TODO Coder (API ?)
    }


    public toString(): string {
        return (this.caseAvant.piece == undefined ? "" : (this.caseAvant.piece.code + " ")) + this.caseAvant.emplacement + " -> " + this.caseApres.emplacement
    }
}