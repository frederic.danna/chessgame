export class Joueur {
    
    private _nom: string
    
    public get nom() : string {
        return this._nom
    }
    

    constructor(nom: string) {
        this._nom = nom
    }
}