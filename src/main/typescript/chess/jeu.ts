import { DeplacementPiece } from './deplacements/deplacement_piece';
import { Echiquier } from './echiquier/echiquier';
import { IHM } from './ihm/ihm';
import { IHMFactory } from './ihm/ihm_factory';
import { Joueur } from './joueur';
import { Piece } from './pieces/piece';

export class Jeu {

    /*
    Constantes commentées pour éviter l'erreur à l'exécution : 
    << TypeError: Class extends value undefined is not a constructor or null >>
    (imports circulaires non possible en TypeScript)
    
    public static readonly TOUR_NOIRE_1: Piece = Piece.TOUR_NOIRE_1
    public static readonly CAVALIER_NOIR_1: Piece = Piece.CAVALIER_NOIR_1
    public static readonly FOU_NOIR_1: Piece = Piece.FOU_NOIR_1
    public static readonly ROI_NOIR: Piece = Piece.ROI_NOIR
    public static readonly REINE_NOIRE: Piece = Piece.REINE_NOIRE
    public static readonly FOU_NOIR_2: Piece = Piece.FOU_NOIR_2
    public static readonly CAVALIER_NOIR_2: Piece = Piece.CAVALIER_NOIR_2
    public static readonly TOUR_NOIRE_2: Piece = Piece.TOUR_NOIRE_2
    public static readonly PION_NOIR_1: Piece = Piece.PION_NOIR_1
    public static readonly PION_NOIR_2: Piece = Piece.PION_NOIR_2
    public static readonly PION_NOIR_3: Piece = Piece.PION_NOIR_3
    public static readonly PION_NOIR_4: Piece = Piece.PION_NOIR_4
    public static readonly PION_NOIR_5: Piece = Piece.PION_NOIR_5
    public static readonly PION_NOIR_6: Piece = Piece.PION_NOIR_6
    public static readonly PION_NOIR_7: Piece = Piece.PION_NOIR_7
    public static readonly PION_NOIR_8: Piece = Piece.PION_NOIR_8

    public static readonly TOUR_BLANCHE_1: Piece = Piece.TOUR_BLANCHE_1
    public static readonly CAVALIER_BLANC_1: Piece = Piece.CAVALIER_BLANC_1
    public static readonly FOU_BLANC_1: Piece = Piece.FOU_BLANC_1
    public static readonly ROI_BLANC: Piece = Piece.ROI_BLANC
    public static readonly REINE_BLANCHE: Piece = Piece.REINE_BLANCHE
    public static readonly FOU_BLANC_2: Piece = Piece.FOU_BLANC_2
    public static readonly CAVALIER_BLANC_2: Piece = Piece.CAVALIER_BLANC_2
    public static readonly TOUR_BLANCHE_2: Piece = Piece.TOUR_BLANCHE_2
    public static readonly PION_BLANC_1: Piece = Piece.PION_BLANC_1
    public static readonly PION_BLANC_2: Piece = Piece.PION_BLANC_2
    public static readonly PION_BLANC_3: Piece = Piece.PION_BLANC_3
    public static readonly PION_BLANC_4: Piece = Piece.PION_BLANC_4
    public static readonly PION_BLANC_5: Piece = Piece.PION_BLANC_5
    public static readonly PION_BLANC_6: Piece = Piece.PION_BLANC_6
    public static readonly PION_BLANC_7: Piece = Piece.PION_BLANC_7
    public static readonly PION_BLANC_8: Piece = Piece.PION_BLANC_8
    */

    private static readonly ihm: IHM = IHMFactory.instance


    private _joueur1: Joueur
    public get joueur1(): Joueur {
        return this._joueur1
    }

    private _joueur2: Joueur
    public get joueur2(): Joueur {
        return this._joueur2
    }

    private _echiquier: Echiquier
    public get echiquier(): Echiquier {
        return this._echiquier
    }

    private _numeroTour: number
    public get numeroTour(): number {
        return this._numeroTour
    }


    constructor(joueur1: Joueur, joueur2: Joueur) {
        this._joueur1 = joueur1
        this._joueur2 = joueur2

        this._echiquier = new Echiquier();

        this._numeroTour = 1;
    }


    public jouerPartie(): void {
        Jeu.ihm.notifier("C'est parti !")

        Jeu.ihm.afficherEchiquier()

        while (!this.estTermine()) {
            Jeu.ihm.majNumeroTour()
            this.jouerUnTour();
        }

        Jeu.ihm.notifier("C'est fini !")
    }


    public estTermine(): boolean {
        return this.estPat() || this.estMat();
        // TODO Aussi le cas où un joueur déclare forfait (?)
    }

    public estPat(): boolean {
        // TODO Coder.
        return false;
    }

    public estMat(): boolean {
        // TODO Coder.
        return false;
    }


    public jouerUnTour(): void {

        this.jouer(this._joueur1);

        if (this.estTermine()) {
            return
        }

        this.jouer(this._joueur2);

        this._numeroTour++
    }

    private jouer(joueur: Joueur): void {
        this.bougerPiece(joueur)
        Jeu.ihm.afficherEchiquier();
    }

    private bougerPiece(joueur: Joueur) {

        let depl: DeplacementPiece = this._saisirCoup(joueur)

        let caseDepart = depl.caseAvant

        if (caseDepart.piece == undefined) {
            throw new Error("La case " + caseDepart.emplacement + " ne contient pas de pièce (vide).")
        }
        let pieceDeplacee: Piece = caseDepart.piece

        let caseArrivee = depl.caseApres
        if (caseArrivee.piece != undefined) {
            throw new Error("La case " + caseArrivee.emplacement + " contient déjà une pièce (" + caseArrivee.piece.code + ").")
        }

        this.echiquier.deplacer(caseDepart, pieceDeplacee, caseArrivee)
    }

    /**
     * @param joueur 
     * @return Le déplacement du joueur. *NB* : Le déplacement est {@link DeplacementPieces.estValide() valide}.
     */
    private _saisirCoup(joueur: Joueur): DeplacementPiece {

        let depl: DeplacementPiece = Jeu.ihm.saisirDeplacementPiece('Quel coup jouez-vous, ' + joueur.nom + ' ?')
        console.log("Coup joué : " + depl.toString() + ".")

        depl.valider // Rq : Ce contrôle est inutile si l'IHM empêche l'utilisateur de saisir un déplacement interdit ("par conception").

        return depl
    }
}