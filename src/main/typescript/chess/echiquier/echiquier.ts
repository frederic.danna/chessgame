import { Piece } from '../pieces/piece';
import { Pieces } from '../pieces/pieces';
import { Case } from './case';

export class Echiquier {

    public static readonly DIMENSION = 8
    public static readonly NBR_CASES = Echiquier.DIMENSION

    public static caseEn(noLig: number, noCol: number): Case {
        if (noLig == 1 && noCol == 1) return Case.A1; if (noLig == 1 && noCol == 2) return Case.B1; if (noLig == 1 && noCol == 3) return Case.C1; if (noLig == 1 && noCol == 4) return Case.D1; if (noLig == 1 && noCol == 5) return Case.E1; if (noLig == 1 && noCol == 6) return Case.F1; if (noLig == 1 && noCol == 7) return Case.G1; if (noLig == 1 && noCol == 8) return Case.H1;
        if (noLig == 2 && noCol == 1) return Case.A2; if (noLig == 2 && noCol == 2) return Case.B2; if (noLig == 2 && noCol == 3) return Case.C2; if (noLig == 2 && noCol == 4) return Case.D2; if (noLig == 2 && noCol == 5) return Case.E2; if (noLig == 2 && noCol == 6) return Case.F2; if (noLig == 2 && noCol == 7) return Case.G2; if (noLig == 2 && noCol == 8) return Case.H2;
        if (noLig == 3 && noCol == 1) return Case.A3; if (noLig == 3 && noCol == 2) return Case.B3; if (noLig == 3 && noCol == 3) return Case.C3; if (noLig == 3 && noCol == 4) return Case.D3; if (noLig == 3 && noCol == 5) return Case.E3; if (noLig == 3 && noCol == 6) return Case.F3; if (noLig == 3 && noCol == 7) return Case.G3; if (noLig == 3 && noCol == 8) return Case.H3;
        if (noLig == 4 && noCol == 1) return Case.A4; if (noLig == 4 && noCol == 2) return Case.B4; if (noLig == 4 && noCol == 3) return Case.C4; if (noLig == 4 && noCol == 4) return Case.D4; if (noLig == 4 && noCol == 5) return Case.E4; if (noLig == 4 && noCol == 6) return Case.F4; if (noLig == 4 && noCol == 7) return Case.G4; if (noLig == 4 && noCol == 8) return Case.H4;
        if (noLig == 5 && noCol == 1) return Case.A5; if (noLig == 5 && noCol == 2) return Case.B5; if (noLig == 5 && noCol == 3) return Case.C5; if (noLig == 5 && noCol == 4) return Case.D5; if (noLig == 5 && noCol == 5) return Case.E5; if (noLig == 5 && noCol == 6) return Case.F5; if (noLig == 5 && noCol == 7) return Case.G5; if (noLig == 5 && noCol == 8) return Case.H5;
        if (noLig == 6 && noCol == 1) return Case.A6; if (noLig == 6 && noCol == 2) return Case.B6; if (noLig == 6 && noCol == 3) return Case.C6; if (noLig == 6 && noCol == 4) return Case.D6; if (noLig == 6 && noCol == 5) return Case.E6; if (noLig == 6 && noCol == 6) return Case.F6; if (noLig == 6 && noCol == 7) return Case.G6; if (noLig == 6 && noCol == 8) return Case.H6;
        if (noLig == 7 && noCol == 1) return Case.A7; if (noLig == 7 && noCol == 2) return Case.B7; if (noLig == 7 && noCol == 3) return Case.C7; if (noLig == 7 && noCol == 4) return Case.D7; if (noLig == 7 && noCol == 5) return Case.E7; if (noLig == 7 && noCol == 6) return Case.F7; if (noLig == 7 && noCol == 7) return Case.G7; if (noLig == 7 && noCol == 8) return Case.H7;
        if (noLig == 8 && noCol == 1) return Case.A8; if (noLig == 8 && noCol == 2) return Case.B8; if (noLig == 8 && noCol == 3) return Case.C8; if (noLig == 8 && noCol == 4) return Case.D8; if (noLig == 8 && noCol == 5) return Case.E8; if (noLig == 8 && noCol == 6) return Case.F8; if (noLig == 8 && noCol == 7) return Case.G8; if (noLig == 8 && noCol == 8) return Case.H8;
        throw new Error("Pas de case en (" + noLig + ", " + noCol + ") : en dehors de l'échiquier.")
    }


    private _cases: Case[][]
    /** Les cases de cet échiquier. */
    public get cases(): Case[][] {
        return this._cases
    }


    public constructor() {

        this._cases = new Array<Case[]>();
        for (let noLigne = 1; noLigne <= Echiquier.NBR_CASES; noLigne++) {
            let ligne = new Array<Case>()
            for (let noColonne = 1; noColonne <= Echiquier.NBR_CASES; noColonne++) {
                ligne.push(Echiquier.caseEn(noLigne, noColonne))
            }
            this._cases.push(ligne)
        }

        this.placerPieces()
    }


    public pieceSur(uneCase: Case): Piece | undefined {
        return uneCase.piece
    }

    private placerPieces() {

        this.positionner(Case.A8, Pieces.TOUR_NOIRE_1)
        this.positionner(Case.B8, Pieces.CAVALIER_NOIR_1)
        this.positionner(Case.C8, Pieces.FOU_NOIR_1)
        this.positionner(Case.D8, Pieces.ROI_NOIR)
        this.positionner(Case.E8, Pieces.REINE_NOIRE)
        this.positionner(Case.F8, Pieces.FOU_NOIR_2)
        this.positionner(Case.G8, Pieces.CAVALIER_NOIR_2)
        this.positionner(Case.H8, Pieces.TOUR_NOIRE_2)
        this.positionner(Case.A7, Pieces.PION_NOIR_1); this.positionner(Case.B7, Pieces.PION_NOIR_2); this.positionner(Case.C7, Pieces.PION_NOIR_3); this.positionner(Case.D7, Pieces.PION_NOIR_4); this.positionner(Case.E7, Pieces.PION_NOIR_5); this.positionner(Case.F7, Pieces.PION_NOIR_6); this.positionner(Case.G7, Pieces.PION_NOIR_7); this.positionner(Case.H7, Pieces.PION_NOIR_8);

        this.positionner(Case.A2, Pieces.PION_BLANC_1); this.positionner(Case.B2, Pieces.PION_BLANC_2); this.positionner(Case.C2, Pieces.PION_BLANC_3); this.positionner(Case.D2, Pieces.PION_BLANC_4); this.positionner(Case.E2, Pieces.PION_BLANC_5); this.positionner(Case.F2, Pieces.PION_BLANC_6); this.positionner(Case.G2, Pieces.PION_BLANC_7); this.positionner(Case.H2, Pieces.PION_BLANC_8);
        this.positionner(Case.A1, Pieces.TOUR_BLANCHE_1)
        this.positionner(Case.B1, Pieces.CAVALIER_BLANC_1)
        this.positionner(Case.C1, Pieces.FOU_BLANC_1)
        this.positionner(Case.D1, Pieces.REINE_BLANCHE)
        this.positionner(Case.E1, Pieces.ROI_BLANC)
        this.positionner(Case.F1, Pieces.FOU_BLANC_2)
        this.positionner(Case.G1, Pieces.CAVALIER_BLANC_2)
        this.positionner(Case.H1, Pieces.TOUR_BLANCHE_2)
    }

    public piece(uneCase: Case): Piece | undefined {
        return uneCase.piece
    }

    public deplacer(caseDepart: Case, pieceDeplacee: Piece, caseArrivee: Case) {
        // TODO Faire une animation.
        this.liberer(caseDepart)
        this.positionner(caseArrivee, pieceDeplacee)
    }


    public liberer(uneCase: Case): void {
        uneCase.enleverPiece()
    }

    public positionner(uneCase: Case, piece: Piece): void {
        uneCase.placerPiece(piece)
    }
}