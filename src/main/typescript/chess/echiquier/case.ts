import { Piece } from '../pieces/piece'
import { Couleur } from '../couleur';

export class Case {

    // TODO Utiliser une 'enum' plutôt.
    public static readonly A8: Case = new Case('A8', Couleur.NOIR); public static readonly B8: Case = new Case('B8', Couleur.BLANC); public static readonly C8: Case = new Case('C8', Couleur.NOIR); public static readonly D8: Case = new Case('D8', Couleur.BLANC); public static readonly E8: Case = new Case('E8', Couleur.NOIR); public static readonly F8: Case = new Case('F8', Couleur.BLANC); public static readonly G8: Case = new Case('G8', Couleur.NOIR); public static readonly H8: Case = new Case('H8', Couleur.BLANC);
    public static readonly A7: Case = new Case('A7', Couleur.BLANC); public static readonly B7: Case = new Case('B7', Couleur.NOIR); public static readonly C7: Case = new Case('C7', Couleur.BLANC); public static readonly D7: Case = new Case('D7', Couleur.NOIR); public static readonly E7: Case = new Case('E7', Couleur.BLANC); public static readonly F7: Case = new Case('F7', Couleur.NOIR); public static readonly G7: Case = new Case('G7', Couleur.BLANC); public static readonly H7: Case = new Case('H7', Couleur.NOIR);
    public static readonly A6: Case = new Case('A6', Couleur.NOIR); public static readonly B6: Case = new Case('B6', Couleur.BLANC); public static readonly C6: Case = new Case('C6', Couleur.NOIR); public static readonly D6: Case = new Case('D6', Couleur.BLANC); public static readonly E6: Case = new Case('E6', Couleur.NOIR); public static readonly F6: Case = new Case('F6', Couleur.BLANC); public static readonly G6: Case = new Case('G6', Couleur.NOIR); public static readonly H6: Case = new Case('H6', Couleur.BLANC);
    public static readonly A5: Case = new Case('A5', Couleur.BLANC); public static readonly B5: Case = new Case('B5', Couleur.NOIR); public static readonly C5: Case = new Case('C5', Couleur.BLANC); public static readonly D5: Case = new Case('D5', Couleur.NOIR); public static readonly E5: Case = new Case('E5', Couleur.BLANC); public static readonly F5: Case = new Case('F5', Couleur.NOIR); public static readonly G5: Case = new Case('G5', Couleur.BLANC); public static readonly H5: Case = new Case('H5', Couleur.NOIR);
    public static readonly A4: Case = new Case('A4', Couleur.NOIR); public static readonly B4: Case = new Case('B4', Couleur.BLANC); public static readonly C4: Case = new Case('C4', Couleur.NOIR); public static readonly D4: Case = new Case('D4', Couleur.BLANC); public static readonly E4: Case = new Case('E4', Couleur.NOIR); public static readonly F4: Case = new Case('F4', Couleur.BLANC); public static readonly G4: Case = new Case('G4', Couleur.NOIR); public static readonly H4: Case = new Case('H4', Couleur.BLANC);
    public static readonly A3: Case = new Case('A3', Couleur.BLANC); public static readonly B3: Case = new Case('B3', Couleur.NOIR); public static readonly C3: Case = new Case('C3', Couleur.BLANC); public static readonly D3: Case = new Case('D3', Couleur.NOIR); public static readonly E3: Case = new Case('E3', Couleur.BLANC); public static readonly F3: Case = new Case('F3', Couleur.NOIR); public static readonly G3: Case = new Case('G3', Couleur.BLANC); public static readonly H3: Case = new Case('H3', Couleur.NOIR);
    public static readonly A2: Case = new Case('A2', Couleur.NOIR); public static readonly B2: Case = new Case('B2', Couleur.BLANC); public static readonly C2: Case = new Case('C2', Couleur.NOIR); public static readonly D2: Case = new Case('D2', Couleur.BLANC); public static readonly E2: Case = new Case('E2', Couleur.NOIR); public static readonly F2: Case = new Case('F2', Couleur.BLANC); public static readonly G2: Case = new Case('G2', Couleur.NOIR); public static readonly H2: Case = new Case('H2', Couleur.BLANC);
    public static readonly A1: Case = new Case('A1', Couleur.BLANC); public static readonly B1: Case = new Case('B1', Couleur.NOIR); public static readonly C1: Case = new Case('C1', Couleur.BLANC); public static readonly D1: Case = new Case('D1', Couleur.NOIR); public static readonly E1: Case = new Case('E1', Couleur.BLANC); public static readonly F1: Case = new Case('F1', Couleur.NOIR); public static readonly G1: Case = new Case('G1', Couleur.BLANC); public static readonly H1: Case = new Case('H1', Couleur.NOIR);


    private _emplacement: string
    public get emplacement(): string {
        return this._emplacement
    }

    private _couleur: Couleur
    public get couleur(): Couleur {
        return this._couleur
    }

    private _piece: Piece | undefined
    public get piece(): Piece | undefined {
        return this._piece
    }


    private constructor(emplacement: string, couleur: Couleur) {
        this._emplacement = emplacement
        this._couleur = couleur // (((numeroLigne - 1) * 8 + numeroColonne + (numeroLigne % 2)) % 2) == 0 ? Couleur.BLANC : Couleur.NOIR
    }


    public enleverPiece(): void {
        if (this._piece == undefined) {
            throw new Error("Case " + this.emplacement + " non occupée.")
        }
        this._piece = undefined
    }

    public placerPiece(piece: Piece): void {
        if (this._piece != undefined) {
            throw new Error("Case " + this.emplacement + " déjà occupée (par un " + this._piece.nom + ").")
        }
        this._piece = piece
    }


    public toString(): string {
        return "(" + this.emplacement + ")[" + this.couleur + "]"
    }
}