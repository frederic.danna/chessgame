import { Piece } from './piece';

export class Tour extends Piece {
    
    public get code(): string {
        return "T"
    }

    public get nom(): string {
        return "Tour"
    }

}