import { Piece } from './piece';

export class Roi extends Piece {

    public get code(): string {
        return "K"
    }

    public get nom(): string {
        return "Roi"
    }

}