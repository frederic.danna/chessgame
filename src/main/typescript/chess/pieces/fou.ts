import { Piece } from './piece';

export class Fou extends Piece {
    
    public get code(): string {
        return "F"
    }

    public get nom(): string {
        return "Fou"
    }

}