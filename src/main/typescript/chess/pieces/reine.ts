import { Piece } from './piece';

export class Reine extends Piece {

    public get code(): string {
        return "Q"
    }

    public get nom(): string {
        return "Reine"
    }

}