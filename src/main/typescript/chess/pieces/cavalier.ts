import { Piece } from './piece';

export class Cavalier extends Piece {

    public get code(): string {
        return "C"
    }

    public get nom(): string {
        return "Cavalier"
    }

}