import { Piece } from './piece';

export class Pion extends Piece {
    
    public get code(): string {
        return "P"
    }

    public get nom(): string {
        return "Pion"
    }

}