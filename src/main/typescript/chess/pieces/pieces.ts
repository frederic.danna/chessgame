import { Couleur } from "../couleur";
import { Cavalier } from './cavalier';
import { Fou } from './fou';
import { Reine } from './reine';
import { Roi } from './roi';
import { Tour } from './tour';
import { Pion } from './pion';

export abstract class Pieces {

    public static readonly TOUR_NOIRE_1: Tour = new Tour(Couleur.NOIR)
    public static readonly CAVALIER_NOIR_1: Cavalier = new Cavalier(Couleur.NOIR)
    public static readonly FOU_NOIR_1: Fou = new Fou(Couleur.NOIR)
    public static readonly ROI_NOIR: Roi = new Roi(Couleur.NOIR)
    public static readonly REINE_NOIRE: Reine = new Reine(Couleur.NOIR)
    public static readonly FOU_NOIR_2: Fou = new Fou(Couleur.NOIR)
    public static readonly CAVALIER_NOIR_2: Cavalier = new Cavalier(Couleur.NOIR)
    public static readonly TOUR_NOIRE_2: Tour = new Tour(Couleur.NOIR)
    public static readonly PION_NOIR_1: Pion = new Pion(Couleur.NOIR)
    public static readonly PION_NOIR_2: Pion = new Pion(Couleur.NOIR)
    public static readonly PION_NOIR_3: Pion = new Pion(Couleur.NOIR)
    public static readonly PION_NOIR_4: Pion = new Pion(Couleur.NOIR)
    public static readonly PION_NOIR_5: Pion = new Pion(Couleur.NOIR)
    public static readonly PION_NOIR_6: Pion = new Pion(Couleur.NOIR)
    public static readonly PION_NOIR_7: Pion = new Pion(Couleur.NOIR)
    public static readonly PION_NOIR_8: Pion = new Pion(Couleur.NOIR)

    public static readonly TOUR_BLANCHE_1: Tour = new Tour(Couleur.BLANC)
    public static readonly CAVALIER_BLANC_1: Cavalier = new Cavalier(Couleur.BLANC)
    public static readonly FOU_BLANC_1: Fou = new Fou(Couleur.BLANC)
    public static readonly REINE_BLANCHE: Reine = new Reine(Couleur.BLANC)
    public static readonly ROI_BLANC: Roi = new Roi(Couleur.BLANC)
    public static readonly FOU_BLANC_2: Fou = new Fou(Couleur.BLANC)
    public static readonly CAVALIER_BLANC_2: Cavalier = new Cavalier(Couleur.BLANC)
    public static readonly TOUR_BLANCHE_2: Tour = new Tour(Couleur.BLANC)
    public static readonly PION_BLANC_1: Pion = new Pion(Couleur.BLANC)
    public static readonly PION_BLANC_2: Pion = new Pion(Couleur.BLANC)
    public static readonly PION_BLANC_3: Pion = new Pion(Couleur.BLANC)
    public static readonly PION_BLANC_4: Pion = new Pion(Couleur.BLANC)
    public static readonly PION_BLANC_5: Pion = new Pion(Couleur.BLANC)
    public static readonly PION_BLANC_6: Pion = new Pion(Couleur.BLANC)
    public static readonly PION_BLANC_7: Pion = new Pion(Couleur.BLANC)
    public static readonly PION_BLANC_8: Pion = new Pion(Couleur.BLANC)

    private constructor() {
    }
}