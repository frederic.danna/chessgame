//// <reference path="../../../../../node_modules/@types/node/readline.d.ts" />

// import readline from 'readline';
import { Couleur } from '../couleur';
import { DeplacementPiece } from '../deplacements/deplacement_piece';
import { Case } from '../echiquier/case';
import { Echiquier } from '../echiquier/echiquier';
import { Jeu } from '../jeu';
import { Piece } from '../pieces/piece';
import { IHM } from './ihm';

export class IHMConsole implements IHM {

    private static readonly LIGNE_SEPARATRICE: boolean = false;

    private static readonly _instance: IHM = new IHMConsole()
    public static get instance(): IHM {
        return this._instance
    }

    // private static readonly _reader: readline.ReadLine = readline.createInterface({
    //     input: process.stdin,
    //     output: process.stdout,
    // })
    private static readonly _deplacementsPieces: DeplacementPiece[] = [
        // Tour n°1:
        new DeplacementPiece(Case.E2, Case.E4),
        new DeplacementPiece(Case.E7, Case.E5),
        // Tour n°2:
        new DeplacementPiece(Case.D2, Case.D3),
        new DeplacementPiece(Case.D7, Case.D6),
    ]

    private _jeu!: Jeu
    public set jeu(jeu: Jeu) {
        this._jeu = jeu
    }


    private constructor() {
    }


    public notifier(message: string): void {
        console.log(message, "\n")
    }


    public saisirDeplacementPiece(message: string): DeplacementPiece {
        let depl: DeplacementPiece | undefined
        // IHMConsole._reader.question(message, (answer: string) => {
        //     reponse = answer
        // })
        console.log(message);
        depl = IHMConsole._deplacementsPieces.shift()
        if (depl == undefined) {
            throw new Error("Plus de mouvement (alors que ni Pat ni Mat ; forfait ?).")
        }
        return depl
    }


    public afficherEchiquier(): void {
        let echiquier: Echiquier = this._jeu.echiquier
        console.log("   A  B  C  D  E  F  G  H ")
        console.log("  .-----------------------.")
        for (let noLig = Echiquier.DIMENSION; noLig >= 1; noLig--) {
            let lig: string = noLig + " "
            for (let noCol = 1; noCol <= Echiquier.DIMENSION; noCol++) {
                let caseEchiquier: Case = Echiquier.caseEn(noLig, noCol)
                let piece: Piece | undefined = echiquier.pieceSur(caseEchiquier)

                lig = lig.concat("|").concat(
                    piece == undefined ?
                        (caseEchiquier.couleur == Couleur.BLANC ? "  " : "##")
                        : (piece.code + (piece.couleur == Couleur.BLANC ? "b" : "n"))
                )
            }
            lig = lig.concat(`| ${noLig}`)
            console.log(lig);

            if (IHMConsole.LIGNE_SEPARATRICE && noLig > 1)
                console.log("  |--+--+--+--+--+--+--+--|")
        }
        console.log("  .-----------------------.")
        console.log("   A  B  C  D  E  F  G  H ")
    }

    public majNumeroTour() {
        console.log('-----------------------------------------------------')
        console.log("Tour n°" + this._jeu.numeroTour + " : ")
    }
}