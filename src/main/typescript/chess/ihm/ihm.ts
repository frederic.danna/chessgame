import { Jeu } from '../jeu';
import { DeplacementPiece } from '../deplacements/deplacement_piece';


export interface IHM {
    
    jeu: Jeu


    /**
     * Affiche une notification aux utilisateurs (joueurs).
     * @param message message à afficher dans la notification
     */
    notifier(message: string): void
    
    saisirDeplacementPiece(message: string): DeplacementPiece


    /**
     * MàJ l'échiquier (le redessine)
     */
    afficherEchiquier(): void

    /**
     * MàJ le n° de tour
     */
    majNumeroTour(): void
}