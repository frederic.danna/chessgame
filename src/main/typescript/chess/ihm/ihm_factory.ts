import { IHM } from './ihm';
import { IHMConsole } from './ihm_console';


export class IHMFactory {

    public static readonly _ihm: IHM = IHMConsole.instance
    public static get instance(): IHM {
        return this._ihm
    }
}