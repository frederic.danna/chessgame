import { IHMFactory } from './chess/ihm/ihm_factory';
import { Jeu } from './chess/jeu';
import { Joueur } from './chess/joueur';
import { IHM } from './chess/ihm/ihm';

const joueur1 = new Joueur('Fred')
const joueur2 = new Joueur('Antoine')
const jeu: Jeu = new Jeu(joueur1, joueur2)

// Injections des dépendances (Injection of Dependencies) :
const ihm: IHM = IHMFactory.instance

// Inversion de contrôle (Inversion of Control) :
ihm.jeu = jeu

jeu.jouerPartie()