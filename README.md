# ChessGame

A Chess game (written in TypeScript)

## Project Management

See [GitLab "ChessGame" project](https://gitlab.com/bugmaker/chessgame) for:

- [code](https://gitlab.com/bugmaker/chessgame/-/tree/master)
- [documentation](https://gitlab.com/bugmaker/chessgame/-/wikis/home)
- [issues](https://gitlab.com/bugmaker/chessgame/issues)
- [releases](https://gitlab.com/bugmaker/chessgame/-/tags)
- etc